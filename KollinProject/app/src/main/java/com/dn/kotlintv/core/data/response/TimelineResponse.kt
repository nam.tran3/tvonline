package com.dn.kotlintv.core.data.response

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class TimelineResponse<T>(
//data class TimelineResponse<T> {
//    @SerializedName("programs")
//    lateinit var programList: List<Programs>
    @SerializedName("programs") val programList: T

)