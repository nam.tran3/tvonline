package com.dn.kotlintv.screens.program

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.PagerAdapter
import com.dn.kotlintv.R
import com.dn.kotlintv.utils.Utils
import kotlinx.android.synthetic.main.tabtable_layout.view.*

class ProgramPagerAdapter(val context: Context, fm: FragmentManager) :
    FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    var viewTab = ArrayList<View>()

    override fun getItem(position: Int): Fragment {
        val fragment = GenreFragment(context, Utils.mGenreList.get(position).genreProgramList)
        return fragment
    }

    override fun getCount(): Int = Utils.mGenreList.size
    override fun getPageTitle(position: Int): CharSequence? {
        return Utils.mGenreList.get(position).genreName
    }

    fun getTabView(position: Int): View{
        var view = LayoutInflater.from(context).inflate(R.layout.tabtable_layout, null)
        view.tabTitle.text = getPageTitle(position)
        viewTab.add(view)
         return view
    }
}