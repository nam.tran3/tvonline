package com.dn.kotlintv.screens.home

import android.content.Intent
import android.graphics.Rect
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dn.kotlintv.R
import com.dn.kotlintv.core.data.local.model.Episode
import com.dn.kotlintv.screens.player.VideoPlayer
import kotlinx.android.synthetic.main.episode_layout.view.*

class LastestAdapter: RecyclerView.Adapter<LastestAdapter.ViewHolder>() {


    companion object{
        private var list = ArrayList<Episode>()
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var layoutInflater = LayoutInflater.from(parent.getContext()).inflate(R.layout.episode_layout, parent, false)

        return ViewHolder(layoutInflater)
    }

    override fun getItemCount(): Int {
        return list!!.size;
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (position < 0 || position > list.size) return

        var episode = list.get(position)
        //Log.i("NAMTH","e=="+episode.title)
        holder.itemView.title_episode.text = episode.title
        holder.itemView.created_episode.text = episode.createBy
        Glide
            .with(holder.itemView.thumbnail_episode.getContext())
            .load(episode.thumbnail)
            .thumbnail(
                Glide
                    .with(holder.itemView.thumbnail_episode.getContext())
                    .load(episode.thumbnail)
            )
            .into(holder.itemView.thumbnail_episode)
    }

    fun addItems(items: List<Episode>) {
        list = ArrayList(items)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {


        init {
            itemView.setOnClickListener(this);
//            itemView.imgFavorite.setOnClickListener(this)
        }
        override fun onClick(v: View?) {

            val intent = Intent(v?.context, VideoPlayer::class.java)
            intent?.putExtra("episodeId", list.get(adapterPosition).videoId)
            v?.context?.startActivity(intent)
        }
    }

    class ItemOffsetDecoration(private val horizontalSpace: Int, private val verticalSpace: Int): RecyclerView.ItemDecoration() {

        override public fun getItemOffsets(
            outRect: Rect,
            view: View,
            parent: RecyclerView,
            state: RecyclerView.State
        ) {
            super.getItemOffsets(outRect, view, parent, state)
            outRect.set(horizontalSpace, 0, horizontalSpace, 0)
        }
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: MutableList<Any>) {
        super.onBindViewHolder(holder, position, payloads)
        //Log.i("NAMTH","vve=="+list.get(position).title)
    }
}