package com.dn.kotlintv.screens.program

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridView

import com.dn.kotlintv.R
import com.dn.kotlintv.core.data.local.model.Episode
import com.dn.kotlintv.core.data.local.model.Program
import kotlinx.android.synthetic.main.fragment_genre.view.*


/**
 * A simple [Fragment] subclass.
 * Use the [GenreFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class GenreFragment(val mContext: Context, val mProgramList: ArrayList<Program>) : Fragment() {
    // TODO: Rename and change types of parameters
    private var mGVGenre: GridView? = null
    private var mAdapter: GenreAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_genre, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mGVGenre = view.gridviewGenre
        mAdapter = GenreAdapter(mContext)
        mAdapter?.addItem(mProgramList)
        mGVGenre?.adapter = mAdapter

    }

    companion object {

    }
}
