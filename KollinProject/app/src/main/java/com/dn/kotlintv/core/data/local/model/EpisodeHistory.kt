package com.dn.kotlintv.core.data.local.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "history")
data class EpisodeHistory(@PrimaryKey @ColumnInfo(name = "videoId") val video_id: String,
                          @ColumnInfo(name = "position") var position: Long,
                          @ColumnInfo(name = "createDate") var create_date: Long)