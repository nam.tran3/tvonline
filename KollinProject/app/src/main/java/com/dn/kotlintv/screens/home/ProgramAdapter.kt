package com.dn.kotlintv.screens.home

import android.content.Context
import android.graphics.Rect
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dn.kotlintv.R
import com.dn.kotlintv.core.data.local.AppDatabase
import com.dn.kotlintv.core.data.local.model.Favorite
import com.dn.kotlintv.core.data.local.model.Program
import kotlinx.android.synthetic.main.program_layout.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*
import kotlin.collections.ArrayList

class ProgramAdapter(var context: Context): RecyclerView.Adapter<ProgramAdapter.ViewHolder>() {

    init {
        mContext = context
        db = AppDatabase(mContext)
    }
    companion object{
        private var currentPosition: Int = 0
        lateinit var mContext: Context
        lateinit var db: AppDatabase
        private var list = ArrayList<Program>()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context).inflate(R.layout.program_layout, parent, false);

        return ViewHolder(inflater);
    }

    override fun getItemCount(): Int {
        return list!!.size;
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (position < 0 || position > list.size) return
        var program = list.get(position)
        var view = holder.itemView
        view.title_program.text = program.title
        view.desc_program.text = program.description
        view.created_program.text = program.createBy

        Glide
            .with(view.getContext())
            .load(program.logo)
            .thumbnail(
                Glide
                    .with(view.getContext())
                    .load(program.logo)
            )
            .into(view.logo_program)

        if (currentPosition != position) {
            view.imgFavorite.setImageDrawable(view.context.resources.getDrawable(R.drawable.ic_tab_favorite_unselected))
        }

        if (program.isFavorite) {
            view.imgFavorite.setImageDrawable(view.context.resources.getDrawable(R.drawable.ic_fav_on))
        } else {
            view.imgFavorite.setImageDrawable(view.context.resources.getDrawable(R.drawable.ic_tab_favorite_unselected))
        }
    }


    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView), View.OnClickListener {

        init {
            itemView.setOnClickListener(this);
             itemView.imgFavorite.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            if (v?.id == R.id.imgFavorite) {
                list.get(adapterPosition).isFavorite = !list.get(adapterPosition).isFavorite
                if (list.get(adapterPosition).isFavorite) {
                    currentPosition = adapterPosition
                    v?.imgFavorite.setImageDrawable(v.context.resources.getDrawable(R.drawable.ic_fav_on))
                }else {
                    v.imgFavorite.setImageDrawable(v.context.resources.getDrawable(R.drawable.ic_tab_favorite_unselected))
                }
                GlobalScope.launch {
                    withContext(Dispatchers.IO) {
                        val item = Favorite(list.get(adapterPosition).programId, Date().time)
                        if (list.get(adapterPosition).isFavorite) {
                            db.getFavoriteDao().insertFavorite(item)
                        } else {
                            db.getFavoriteDao().delete(item)
                        }
                    }

                }
            }

        }

    }
    fun addItems(items: ArrayList<Program>) {
        list?.addAll(items)
    }

    class ItemOffsetDecoration(private val horizontalSpace: Int, private val verticalSpace: Int): RecyclerView.ItemDecoration() {

        override public fun getItemOffsets(
            outRect: Rect,
            view: View,
            parent: RecyclerView,
            state: RecyclerView.State
        ) {
            super.getItemOffsets(outRect, view, parent, state)
            outRect.set(horizontalSpace, 0, horizontalSpace, 0)
        }
    }
}