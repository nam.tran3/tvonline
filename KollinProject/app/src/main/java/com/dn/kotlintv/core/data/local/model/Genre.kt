package com.dn.kotlintv.core.data.local.model

class Genre(val genreName: String, val genreProgramList: ArrayList<Program>)