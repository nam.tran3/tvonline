package com.dn.kotlintv.core.data.local.model

import com.google.gson.annotations.SerializedName

data class EditorChoice ( @SerializedName("programId") val programId : String)
