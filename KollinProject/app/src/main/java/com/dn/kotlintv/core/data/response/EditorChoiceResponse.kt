package com.dn.kotlintv.core.data.response

import androidx.annotation.Keep
import com.dn.kotlintv.core.data.local.model.EditorChoice
import com.google.gson.annotations.SerializedName
@Keep
data class EditorChoiceResponse(
    @SerializedName("editorChoice") val editorChoiceList: List<EditorChoice>

)