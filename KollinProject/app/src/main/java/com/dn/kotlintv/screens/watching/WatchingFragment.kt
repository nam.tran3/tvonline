package com.dn.kotlintv.screens.watching

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.dn.kotlintv.R
import com.dn.kotlintv.core.data.local.AppDatabase
import com.dn.kotlintv.core.data.local.model.Episode
import com.dn.kotlintv.core.data.local.model.EpisodeHistory
import com.dn.kotlintv.screens.utils.Utils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.collections.ArrayList

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [WatchingFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class WatchingFragment : Fragment(), View.OnClickListener {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var mRCFavorite: RecyclerView? = null
    private var mWatchingAdapter: WatchingAdapter? = null
    private var db: AppDatabase? = null
    private var mHistoryList: ArrayList<EpisodeHistory>? = null
    private var mEditLayout: RelativeLayout? = null
    private var mSelectAll: TextView? = null
    private var mDelete: TextView? = null
    private var mIsSelectAll = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        db = getContext()?.let { AppDatabase(it) }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_watching, container, false)
    }

    override fun onViewCreated(rootView: View, savedInstanceState: Bundle?) {
        super.onViewCreated(rootView, savedInstanceState)
        mRCFavorite = rootView.findViewById(R.id.rcFavorite)
        mEditLayout = rootView.findViewById(R.id.edit_layout)
        mSelectAll = rootView.findViewById(R.id.select_all)
        mSelectAll?.setOnClickListener(this)
        mDelete = rootView.findViewById(R.id.delete)
        mDelete?.setOnClickListener(this)
        mWatchingAdapter = WatchingAdapter()
        mRCFavorite?.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        mRCFavorite?.adapter = mWatchingAdapter
        mWatchingAdapter?.addItems(com.dn.kotlintv.utils.Utils.mRecentlyList)
//        viewLifecycleOwner?.let { db?.getEpisodeHistoryDao()?.observerEpisodes()?.observe(it, androidx.lifecycle.Observer {
//            mHistoryList = ArrayList(it)
//            Log.i("NAMTH", "viewLifecycleOwner it=" + it.size)
//
//            mWatchingAdapter?.notifyDataSetChanged()
//            Log.i("NAMTH", "viewLifecycleOwner mWatchingList=" + com.dn.kotlintv.utils.Utils.mRecentlyList)
//
//        }) };
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment WatchingFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            WatchingFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.select_all) {
            mIsSelectAll = !mIsSelectAll
            mSelectAll?.text = if (mIsSelectAll) getString(R.string.txt_unselect_all) else getString(R.string.txt_select_all)
            mWatchingAdapter?.setSelectAll(mIsSelectAll)
        } else if (v?.id == R.id.delete) {
            var count = 0
            var listUpdate = mHistoryList?.let { ArrayList(it) }
            for (fa in com.dn.kotlintv.utils.Utils.mRecentlyList) {

                var episodeId = fa.videoId
                if (listUpdate != null) {
                    for (watchSave in listUpdate) {
                        if (episodeId.equals(watchSave.video_id)) {
                            Log.i("NAMTH","remove nef")
                            listUpdate.remove(watchSave)
                            count++
                            break
                        }
                    }
                }
            }

            Log.i("NAMTH","listUpdate="+listUpdate?.size+":count="+count)
            GlobalScope.launch {
                withContext(Dispatchers.IO) {

                    if (count > 0 ) {
                        db?.getEpisodeHistoryDao()?.deleteAll()
                        if (!listUpdate?.isEmpty()!!) {
                            db?.getEpisodeHistoryDao()?.insertEpisodeList(listUpdate)
                        }
                    }

                }

            }
            mWatchingAdapter?.notifyDataSetChanged()
        }
    }

    fun setViewType(type: Boolean) {
        mEditLayout?.visibility = View.GONE
        if (type) {
            mEditLayout?.visibility = View.VISIBLE
        }
        mWatchingAdapter?.setVIEW_TYPE(type)
    }
}
