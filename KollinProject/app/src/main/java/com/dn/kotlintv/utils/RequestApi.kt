package com.dn.kotlintv.utils

import android.content.Context
import android.text.Html
import android.util.Log
import com.dn.kotlintv.MainViewModel
import com.dn.kotlintv.core.data.local.AppDatabase
import com.dn.kotlintv.core.data.local.model.*
import com.dn.kotlintv.utils.Utils.Companion.mAllEpisodeList
import com.dn.kotlintv.utils.Utils.Companion.mEditorChoiceList
import com.dn.kotlintv.utils.Utils.Companion.mFinishEditorChoiceRequest
import com.dn.kotlintv.utils.Utils.Companion.mFinishFavoriteRequest
import com.dn.kotlintv.utils.Utils.Companion.mFinishGenreRequest
import com.dn.kotlintv.utils.Utils.Companion.mFinishRecentlyRequest
import com.dn.kotlintv.utils.Utils.Companion.mFinishRecommendRequest
import com.dn.kotlintv.utils.Utils.Companion.mFinishTimelineRequest
import com.dn.kotlintv.utils.Utils.Companion.mGenreList
import com.dn.kotlintv.utils.Utils.Companion.mProgramList
import com.dn.kotlintv.utils.Utils.Companion.mRecommendationList
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import kotlinx.coroutines.*
import org.greenrobot.eventbus.EventBus
import retrofit2.HttpException
import java.io.*
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.coroutines.CoroutineContext


class RequestApi(var context: Context, var mVM: MainViewModel) {
    init {
        mainVM = mVM
        mContext = context
        db = AppDatabase(mContext)
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4796-cach-nau-canh-kim-chi-han-quoc-nong-hoi-thom-ngon.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4795-sinh-to-baobab-viet-quat-bo-sung-vitamin-c-hieu-qua.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4794-tom-chien-pesto-voi-toi-va-khoai-tay-day-dinh-duong.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4793-tra-sua-chai-latte-suoi-am-co-the-ngay-gio-mua.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4792-uc-vit-khoai-tay-sot-chanh-leo-hap-dan-nhu-nha-hang.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4791-kem-vien-chien-cuc-hot-mua-dong.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4790-cong-thuc-mi-y-tom-sot-kem-thom-ngay-kho-cuong.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4789-salad-ga-thai-ca-rot-thanh-dam-va-tot-cho-suc-khoe.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4788-canh-ga-chien-nuoc-mam-dam-duoi-bua-com-gia-dinh.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4787-sinh-to-matcha-xoai-la-mieng-bo-duong.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4786-nau-mien-luon-ngon-khong-thua-gi-nha-hang.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4785-tu-lam-trung-cut-lon-xao-me-an-vat-cuc-vui-mieng.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4784-cong-thuc-sup-ca-chua-thom-ngay-bo-duong.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4783-trung-op-let-kieu-tay-ban-nha-dam-da-huong-vi.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4782-lam-keo-gay-ngo-nghinh-cho-dem-giang-sinh-them-ngot-ngao.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4780-mi-chay-yakisoba-chua-ngot-ngon-het-cho-che.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4779-cong-thuc-banh-tra-sua-tran-chau-duong-den-noi-nhu-con.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4778-mi-ca-ri-chay-dam-da-khong-sao-cuong-lai-duoc.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4777-mojito-mam-xoi-bo-sung-vitamin-lai-thu-gian-hieu-qua.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4776-tu-lam-tra-tao-que-an-toan-cho-suc-khoe.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4775-canh-ga-vien-thom-ngon-boi-bo-mua-dong.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4774-banh-tra-earl-grey-chanh-sang-chanh-nhu-quy-toc.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4773-muc-xao-sot-compote-dam-da-tran-ngap-huong-vi.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4772-tu-lam-banh-xeo-vang-rum-thom-gion-hap-dan-tai-nha.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4771-hai-san-xao-dua-chuot-vua-dam-da-vua-thanh-mat.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4770-trung-kho-vua-dam-da-vua-thom-ngon-ly-tuong.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4769-mon-bun-moc-hap-dan-khong-ai-cuong-lai-duoc.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4768-banh-souffle-kieu-phap-vi-socola-ngot-ngao-ngat-ngay.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4767-sup-khoai-tay-pho-mai-huong-vi-tuyet-cu-meo-ai-cung-phai-me.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4766-com-ga-toi-voi-rau-mui-thom-ngon-nhu-nha-hang.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4765-banh-cam-sua-chua-don-gian-ma-cuc-ky-thom-ngon-doc-dao.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4764-mi-thit-bo-kieu-trung-quoc-am-ap-ngay-dong.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4763-sandwich-ga-voi-salad-ngon-khong-cuong-lai-duoc.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4762-thit-kho-trung-dua-tan-chay-trong-mieng.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4761-nem-cuon-rau-cu-thanh-dam-ma-ngon-ngat-ngay.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4760-nikujaga-thit-bo-ham-khoai-tay-nhat-ban.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4759-tom-chien-toi-voi-ca-rot-va-bi-xanh-don-gian-thom-ngon.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4758-mi-soba-ca-hoi-nhat-ban-sot-matcha-co-mot-khong-hai-thom-lung.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4757-trung-ran-pho-mai-voi-giam-bong-day-nang-luong.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4756-cong-thuc-cocktail-trung-sua-thom-ngay-am-ap.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4755-banh-panna-cotta-chuan-huong-vi-y-ngon-khong-thua-gi-nha-hang.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4754-mi-ga-miso-bo-duong-it-beo-lam-nen-bua-trua-tien-loi.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4753-lam-banh-brownie-socola-it-beo-de-tha-ho-an-ma-khong-so-tang-can.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4752-cong-thuc-sot-pho-mai-beo-ngay-lam-gia-vi-cho-cac-mon-chinh-hap-dan.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4751-ca-nuong-nguyen-con-voi-xuc-xich-hanh-tay-do-va-ca-chua.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4750-ga-bulbogi-dam-da-huong-vi-han-quoc-dinh-cao.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4749-mi-ca-hoi-teriyaki-kieu-nhat-doc-dao-cho-co-the-khoe-manh.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4748-ga-sot-kem-nam-ngon-het-cho-che-cho-bua-toi-hap-dan.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4747-lam-mi-y-sot-ca-chua-va-thit-vien-ga-tay-ngon-dung-dieu.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4746-huong-dan-lam-cocktail-ruou-vang-do-cho-bua-tiec-mua-dong.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4745-banh-mi-chuoi-kem-pho-mai-vua-bo-duong-vua-tuyet-ngon.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4744-dam-da-hap-dan-voi-mon-thit-bo-luc-lac-ngon-dung-dieu.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4743-bun-rieu-cua-tro-nen-cuc-de-lam-voi-cong-thuc-nay.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4742-banh-tao-caramel-lat-nguoc-ngot-ngao-cho-ngay-mua-dong.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4741-vao-bep-che-bien-mon-trung-bi-ngo-hap-dan-don-halloween.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4740-cung-lam-mon-sup-khoai-tay-am-ap-ngay-dong.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4739-cong-thuc-tom-xao-mang-tay-it-calo-vua-ngon-mieng-vua-giu-dang-tuyet-hao.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4738-dem-huong-vi-phap-ve-nha-voi-banh-macaron-tra-xanh-thanh-mat.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4737-mach-ban-mon-com-gau-con-dap-chan-trung-ran-vua-ngon-vua-de-thuong.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4736-com-rang-ga-teriyaki-ngon-mieng-hap-dan-cho-ngay-cuoi-tuan.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4735-banh-kep-salad-ga-bo-duong-va-tien-loi-cho-bua-trua.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4734-dep-da-thon-dang-voi-salad-hoa-qua-mua-dong-cuc-hap-dan.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4733-cong-thuc-banh-mousse-socola-gay-nghien-tren-toan-the-gioi.html")
        listRequest.add("http://www.amthuc365.vn/cong-thuc/4732-thom-nuc-mui-mon-tom-xao-chanh-toi-ngon-het-cho-che.html")
    }
    companion object {
        lateinit var mainVM: MainViewModel
        lateinit var mContext: Context
        private val parentJob = Job()
        //create a coroutine context with the job and the dispatcher
        private val coroutineContext : CoroutineContext get() = parentJob + Dispatchers.Default
        //create a coroutine scope with the coroutine context
        private val scope = CoroutineScope(coroutineContext)
        private var mProgramResponse = HashMap<String, Program>()
        private var mRecommandationResponse = ArrayList<String>()
        private var mEditorChoiceResponse = ArrayList<EditorChoice>()
        private var mGenreResponse = ArrayList<GenreItem>()
        private var mFavoriteSave = ArrayList<Favorite>()
        private var mRecentlyHistoryList = ArrayList<EpisodeHistory>()
        lateinit var db: AppDatabase
//        private var mAllEpisodeList = ArrayList<Episode>()
        private var listRequest = ArrayList<String>();
        fun fetchTimeline() {
            scope.launch {
                withContext(Dispatchers.IO) {
                    try {
                        val request = mainVM.fetchTimeline()
                        mProgramResponse = request.programList.associateBy { it.programId} as HashMap<String, Program>
                        mFinishTimelineRequest = true
//                        Log.i("NAMTH","getTimeline1 request="+mProgramResponse)
                        processData()

                    }catch (e: HttpException) {
                        Log.i("NAMTH","getTimeline1 Errr 11")
                    }catch (e: Throwable) {
                        // Log error //)
                        Log.i("NAMTH","getTimeline1 Errr="+e.toString())
                        mFinishTimelineRequest = true
                        processData()
                    }
                }
            }
        }

        fun fetchRecommendation() {
            scope.launch {
                withContext(Dispatchers.IO) {
                    try {
                        val request = mainVM.fetchRecommendation1()
                        mRecommandationResponse = request.recommendations as ArrayList<String>
                        mFinishRecommendRequest = true
                        Log.i("NAMTH","fetchRecommendation request="+request.recommendations)
                        processData()

                    }catch (e: HttpException) {
                        Log.i("NAMTH","fetchRecommendation Errr 11")
                    }catch (e: Throwable) {
                        // Log error //)
                        Log.i("NAMTH","fetchRecommendation Errr")
                        mFinishRecommendRequest = true
                        processData()
                    }
                }
            }
        }

        fun fetchEditorChoice() {
            scope.launch {
                withContext(Dispatchers.IO) {
                    try {
                        val request = mainVM.fetchEditorChoice()
                        mEditorChoiceResponse = request.editorChoiceList as ArrayList<EditorChoice>
                        mFinishEditorChoiceRequest = true
                        Log.i("NAMTH","fetchEditorChoice request="+request.editorChoiceList)
                        processData()

                    }catch (e: HttpException) {
                        Log.i("NAMTH","fetchEditorChoice Errr 11")
                    }catch (e: Throwable) {
                        // Log error //)
                        Log.i("NAMTH","fetchEditorChoice Errr")
                        mFinishEditorChoiceRequest = true
                        processData()
                    }
                }
            }
        }

        fun fetchFavorite() {
            scope.launch {
                withContext(Dispatchers.IO) {
                    try {
                        mFavoriteSave = db?.getFavoriteDao()?.getAll() as ArrayList<Favorite>
                        mFinishFavoriteRequest = true
                        Log.i("NAMTH","mFavoriteSave request="+mFavoriteSave.size)
                        processData()

                    }catch (e: HttpException) {
                        Log.i("NAMTH","fetchGenre Errr 11")
                    }catch (e: Throwable) {
                        // Log error //)
                        Log.i("NAMTH","fetchGenre Errr="+e.toString())
                        mFinishFavoriteRequest = true
                        processData()
                    }
                }
            }
        }

        fun fetchWatching() {
            db.getEpisodeHistoryDao().observerEpisodes().observeForever(androidx.lifecycle.Observer {
                Log.i("NAMTH","fetchWatching="+it.size)
                mRecentlyHistoryList = ArrayList(it)
                mFinishRecentlyRequest = true
                processData()
            })
        }

        fun fetchGenre() {
            scope.launch {
                withContext(Dispatchers.IO) {
                    try {
                        val request = mainVM.fetchGenre()
                        mGenreResponse = request.genreList as ArrayList<GenreItem>
                        mFinishGenreRequest = true
                        Log.i("NAMTH","fetchGenre request="+request.genreList.size)
                        processData()

                    }catch (e: HttpException) {
                        Log.i("NAMTH","fetchGenre Errr 11")
                    }catch (e: Throwable) {
                        // Log error //)
                        Log.i("NAMTH","fetchGenre Errr="+e.toString())
                        mFinishGenreRequest = true
                        processData()
                    }
                }
            }
        }

        fun fetchData() {
            scope.launch {
                withContext(Dispatchers.IO) {
                    var count = 0
                    var listPatch = ArrayList<String>()
                    for (i in 161..199
                    ) {
                        count ++
                        Log.i("NAMTH","count="+count)
                        var url1 = URL("http://www.amthuc365.vn/cong-thuc/91-loai-mon/trang-"+i+"/")
                        var buffer1 = BufferedReader(InputStreamReader(url1.openStream()))
                        var inputLine1 = StringBuilder()
                        var line1 = buffer1.readLine()
                        while (line1 != null) {
                            inputLine1.append(line1)
                            line1 = buffer1.readLine()

                        }
                        var pattern1 = Pattern.compile("<div class=\"box-recipe_bottom\">(.*?)<!--end it-new--> <div class=\"clearfix\">")
                        var matcher1 = pattern1.matcher(inputLine1);
                        if (matcher1.find()) {
                            var body1 = matcher1.group(1)
                            body1 = body1.replace("<!--end it-new-->","\n")
                            var arrThanhphan1 = body1.split("\n").toTypedArray()
                            arrThanhphan1.forEach {
//                                Log.i("NAMTH","par a="+it)
                                pattern1 = Pattern.compile("href=\"(.*?)><image")
                                matcher1 = pattern1.matcher(it);
                                if (matcher1.find()) {

                                    var index1= matcher1.group(1).indexOf("\" title")
                                    var value1 = matcher1.group(1).substring(0, index1)

                                    listPatch.add("http://www.amthuc365.vn" + value1)
                                }
                            }

                        }else {

                        }
                    }

                    var list = ArrayList<ChiTiet>()
                    for (str in listPatch) {
                        var jsonData = try {
                            var title = ""
                            var description = ""
                            var thumbnail = ""
                            var componentList = ArrayList<String>()
                            var tutorialList = ArrayList<String>()
                            var listPatch = ArrayList<String>()
//                         mainVM.fetchData().string()

                            var url = URL(str)
                            var buffer = BufferedReader(InputStreamReader(url.openStream()))
                            var inputLine = StringBuilder()
                            var inputLine1 = StringBuilder()
                            var line = buffer.readLine()
                            while (line != null) {
                                inputLine.append(line)
                                line = buffer.readLine()

                            }

                            var index = inputLine.indexOf("<div class=\"box-video_info\"> <h1>")
                            inputLine1.append(inputLine?.substring(index, inputLine.length))
                            var strend = "</div> </li> </ul> </div> </div> </div><!--end box-recipe_content-->";
                            index = inputLine1.indexOf("</div> </li> </ul> </div> </div> </div><!--end box-recipe_content-->")
                            var res = inputLine1.substring(0, index + strend.length)
                            var pattern = Pattern.compile("<h1>(.*?)</h1>")
                            var matcher = pattern.matcher(res);
                            if (matcher.find())
                            {
//                            Log.i("NAMTH","fetchData title="+matcher.group(1))
                                title = Html.fromHtml(matcher.group(1)).toString()
                            }
                            pattern = Pattern.compile("<div class=\"info-intro\">(.*?)</div> </div><!--end detail-info-->")
                            matcher = pattern.matcher(res);
                            if (matcher.find())
                            {
//                            Log.i("NAMTH","fetchData 222="+Html.fromHtml(matcher.group(1)).toString())
                                description = Html.fromHtml(matcher.group(1)).toString()
                            }

                            pattern = Pattern.compile("<img class=\"img-responsive\" alt=\"\" src=\"(.*?)\"> </div> <div class=\"loader-video\">")
                            matcher = pattern.matcher(res);
                            if (matcher.find())
                            {
//                            Log.i("NAMTH","fetchData 333="+matcher.group(1))
                                thumbnail = Html.fromHtml(matcher.group(1)).toString()
                            }

                            pattern = Pattern.compile("<h3 class=\"h-recipe\">(.*?)</h3> <ul class=\"menu-ingredients\"> <li>")
                            matcher = pattern.matcher(res);
                            if (matcher.find())
                            {
//                            Log.i("NAMTH","fetchData 444="+matcher.group(1))
                            }

                            pattern = Pattern.compile("<ul class=\"menu-ingredients\"> <li>(.*?)</a> </li> </ul> </div> <div class=\"col-md-8\">")
                            matcher = pattern.matcher(res);
                            if (matcher.find())
                            {
                                var thanhphan = matcher.group(1)
                                thanhphan = thanhphan.replace("</a> </li> <li>","\n")
                                var arrThanhphan = thanhphan.split("\n").toTypedArray()
                                arrThanhphan.forEach {
                                    var firstIndex = it.indexOf("<a")
                                    var endIndex = it.indexOf("blank\">")
                                    var fiterHtml = it.removeRange(firstIndex, endIndex+("blank\">").length)
                                    fiterHtml = fiterHtml.replace("\\s+".toRegex(), " ")
//                                Log.i("NAMTH","fetchData 55522="+fiterHtml)
                                    componentList.add(Html.fromHtml(fiterHtml).toString())
                                }
                            }

                            pattern = Pattern.compile("<div class=\"col-md-8\"> (.*?)</div> </li> </ul> </div> </div> </div>")
                            matcher = pattern.matcher(res);
                            if (matcher.find()) {
                                var huongdan = matcher.group(1)
                                pattern = Pattern.compile("<h3 class=\"h-recipe\">(.*?)</h3>")
                                matcher = pattern.matcher(huongdan);
                                if (matcher.find()) {
//                                Log.i("NAMTH","fetchData 66611="+matcher.group(1))
                                    var index = huongdan.indexOf("<ul class=\"menu-directions\">") + ("<ul class=\"menu-directions\">").length
                                    huongdan = huongdan.removeRange(0, index)
                                    huongdan = huongdan.replace("</div> </li>","\n")
                                    var arrHuongdan = huongdan.split("\n").toTypedArray()
                                    arrHuongdan.forEach {
                                        var firstIndex = it.indexOf("<li")
                                        var endIndex = it.indexOf("it-intro\">")
                                        var fiterHtml = it.removeRange(firstIndex, endIndex+("it-intro\">").length)
                                        fiterHtml = fiterHtml.replace("\\s+".toRegex(), " ")
//                                    Log.i("NAMTH","fetchData 66622="+Html.fromHtml(fiterHtml).toString() )
                                        tutorialList.add(Html.fromHtml(fiterHtml).toString())
                                    }
                                } else {}
                            } else {

                            }


                            var item = ChiTiet(title, description, thumbnail, componentList, tutorialList)
                            list.add(item)

//
                        }catch (e: HttpException) {
                            Log.i("NAMTH","fetchData Errr 11")
                        }catch (e: Throwable) {
                            Log.i("NAMTH","count="+count+":fetchData Errr="+e.toString())
                        }
                    }
                    var obj = MonAn(list)
                    val builder = GsonBuilder()
                    builder.setPrettyPrinting()
                    val gson = builder.create()
                    var jsonString = gson.toJson(obj);

                    val file = File(mContext.filesDir, "monan.txt")
                    file.createNewFile()
                    file.appendText(jsonString)
                    val readResult = FileInputStream(file).bufferedReader().use { it.readText() }
                    Log.i("NAMTH","finish request")
                }
            }
        }

        private fun processData() {
            if (isRequestFinish()) {
                resetRequest()
                if (!mProgramResponse.isEmpty()) {
                    mAllEpisodeList.clear()
                    mProgramList.clear()
                    mRecommendationList.clear()
                    //remove program havent episode
                    Log.i("NAMTH","processData mProgramResponse="+mProgramResponse.size)
                    val dateTimeFormat = "yyyy-MM-dd HH:mm:ss"
                    val simpleDateFormat = SimpleDateFormat(dateTimeFormat)

                    for (program in mProgramResponse) {
                        Log.i("NAMTH","processData program.value.episodes.isEmpty()="+program.value.episodes.isEmpty())
                        for (e in program.value.episodes) {
                            val endDate: Date = simpleDateFormat.parse(e.end_date)
                            val currentDate: Date = Date()
                            e.createBy = program.value.createBy
                            if (endDate.time < currentDate.time) {
                                program.value.episodes.remove(e)
                            }
                        }

                        if (program.value.episodes.isEmpty()) {
                            mProgramResponse.remove(program.key)
                        } else {
                            mAllEpisodeList.addAll(program.value.episodes)
                        }

                    }
                    Log.i("NAMTH","111processData mAllEpisodeList="+mAllEpisodeList.size)
                    mProgramList = mProgramResponse
                    parseRecommendation(mAllEpisodeList)
                    parseFavorite(mFavoriteSave, mProgramList)
                    parseEditorChoice(mProgramResponse)
                    parseGenre(mProgramResponse)
//                    parseRecently(mAllEpisodeList)

                    Log.i("NAMTH","processData mAllEpisodeList="+mAllEpisodeList.size)
                    Log.i("NAMTH","processData mAllEpisodeList="+mRecommendationList)

                    EventBus.getDefault().post(MessageEvent("onDrawUI"))
                    EventBus.getDefault().post(MessageEvent("finishLoading"))
                }

            }
        }

        private fun parseRecently(listAll: ArrayList<Episode>) {
            if (!mRecentlyHistoryList.isEmpty()) {
                for (his in mRecentlyHistoryList) {
                    val videoId = his.video_id
                    for (e in listAll) {
                        if (videoId.equals(e.videoId)) {
                            Utils.mRecentlyList.add(e)
                        }
                    }
                }
            }
        }

        private fun parseGenre(programList :  HashMap<String, Program>) {
            if (!mGenreResponse.isEmpty()) {
                val genreListSort = mGenreResponse.sortedWith(compareBy({it.genrePosition}))

                for (gen in genreListSort) {
                    val genreName = gen.genreName
                    val list = ArrayList<Program>()
                    for (program in programList) {
                        if (genreName.equals(program.value.genre) || genreName.toLowerCase().equals("all")) {
                            list.add(program.value)
                        }
                    }
                    val genre = Genre(genreName, list)
                    mGenreList.add(genre)
                }
                Log.i("NAMTH","processData parseGenre="+ mGenreList.size)
                for (g in mGenreList) {
                    Log.i("NAMTH","processData g="+g.genreName+":list="+g.genreProgramList.size )
                }
            }
        }
        private fun parseEditorChoice(programList :  HashMap<String, Program>) {
            if (!mEditorChoiceResponse.isEmpty()) {
                for (editorChoice in mEditorChoiceResponse) {
                    programList.get(editorChoice.programId)?.let { mEditorChoiceList.add(it) }
                }
                Log.i("NAMTH","processData mEditorChoiceList="+ mEditorChoiceList.size)
            }
        }

        private fun parseRecommendation(listEpisode: List<Episode>) {
            for (e in listEpisode) {
                if (mRecommandationResponse.contains(e.videoId)) {
                    mRecommendationList.add(e)
                }
            }
        }

        private fun parseFavorite(favorites: ArrayList<Favorite>, programList :  HashMap<String, Program>) {
            for (fa in favorites) {
                var id = fa.program_id
                if (programList.containsKey(id)) {
                    programList.get(id)?.isFavorite = true
                }
            }
        }

        private fun isRequestFinish(): Boolean {
            return mFinishRecommendRequest && mFinishTimelineRequest && mFinishEditorChoiceRequest
                    && mFinishGenreRequest && mFinishFavoriteRequest && mFinishRecentlyRequest
        }

        private fun resetRequest() {
            mFinishRecommendRequest = false
            mFinishTimelineRequest = false
            mFinishEditorChoiceRequest = false
            mFinishGenreRequest = false
            mFinishFavoriteRequest = false
            mFinishRecentlyRequest = false
        }
    }

}
