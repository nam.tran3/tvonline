package com.dn.kotlintv.screens.player

import android.content.Context
import android.content.res.Resources
import android.os.Handler
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.widget.ImageButton
import android.widget.SeekBar
import android.widget.TextView
import android.widget.VideoView
import androidx.annotation.AttrRes
import androidx.constraintlayout.widget.ConstraintLayout
import com.dn.kotlintv.R
import kotlinx.android.synthetic.main.video_controller.view.*

class VideoController: ConstraintLayout, View.OnClickListener, View.OnTouchListener {
    private lateinit var mSeekbar: SeekBar
    lateinit var mCurrentTime: TextView
    lateinit var mDurationTime: TextView
    lateinit var mBtnPre: ImageButton
    lateinit var mBtnFastForward: ImageButton
    lateinit var mBtnPause: ImageButton
    lateinit var mVideoView: VideoView
    val TIME_SEEK = 10000
    val INVISIBLE_DELAY: Long = 3000
    var mHandler: Handler = Handler()
    var mHideRunnable: Runnable? = null
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        // レイアウトXML適用
        val layout: View =
            LayoutInflater.from(context).inflate(R.layout.video_controller, this)
        initView(layout)
    }
    constructor(context: Context, attrs: AttributeSet?,
                @AttrRes defStyleAttr: Int) : super(context, attrs, defStyleAttr)


    private fun initView(v: View) {
        mSeekbar = v.findViewById(R.id.seekBar) as SeekBar
        mCurrentTime = v.findViewById(R.id.current_time) as TextView
        mDurationTime = v.findViewById(R.id.duration_time) as TextView
        mBtnPre = v.findViewById(R.id.btnpre) as ImageButton
        mBtnFastForward = v.findViewById(R.id.btn_fast_forward) as ImageButton
        mBtnPause = v.findViewById(R.id.btnpause) as ImageButton

//        mBtnPre.setOnTouchListener(this)
        mBtnPre.setOnClickListener(this)
        mBtnFastForward.setOnClickListener(this)
        mBtnPause.setOnClickListener(this)
        mSeekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                Log.i("NAMTH","seekbar="+seekBar?.progress)
                var time:Int? = seekBar?.progress
                time?.let { setProgress(it) }
            }

        })
    }

    fun setDuration(duration: Int) {
        mSeekbar.max = duration
    }

    fun setProgress(progress: Int) {
        mSeekbar.setProgress(progress)
        mVideoView?.seekTo(mSeekbar.progress)
    }

    fun setCurrentTime(str: String) {
        mCurrentTime.text = str
    }

    fun setDurationTime(str: String) {
        mDurationTime.text = str
    }

    fun setVideoView(v: VideoView) {
        mVideoView = v
    }
    override fun onClick(v: View?) {
        when(v?.id) {
            R.id.btnpre -> {
                val timeSeek = mVideoView.currentPosition - TIME_SEEK;
                if (timeSeek <= 0)
                    mVideoView.seekTo(0)
                else
                    mVideoView.seekTo(timeSeek)
            }
            R.id.btn_fast_forward -> {
                val timeSeek = mVideoView.currentPosition + TIME_SEEK;
                if (timeSeek >= mVideoView.duration)
                    mVideoView.seekTo(mVideoView.duration)
                else
                    mVideoView.seekTo(timeSeek)
            }
            R.id.btnpause -> {
                if (mVideoView.isPlaying) {
                    show(false)
                    mVideoView.pause()
                    visibility = View.VISIBLE

                } else {
                    if(mVideoView.currentPosition == mVideoView.duration) {
                        mVideoView.seekTo(0)
                    }
                    mVideoView.start()
                    hide()
                }

                switchPlayButton(mVideoView.isPlaying)
            }
            R.id.seekBar -> {

            }
        }
    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        when(v?.id) {
            R.id.btnpre -> {

            }
            R.id.btn_fast_forward -> {
                mVideoView.seekTo(5000)
            }
            R.id.btnpause -> {

            }
        }
        return v?.onTouchEvent(event) ?: true
    }

     fun abortHide() {
        Log.i("NAMTH","abortHide mHideRunnable="+mHideRunnable)
        if (mHideRunnable != null) {
            Log.i("NAMTH","removeCallbacks")
            handler.removeCallbacks(mHideRunnable)
            mHideRunnable = null
        }
    }
//    private fun postHide() {
//        if (hideRunnable != null)
//            handler.removeCallbacks(hideRunnable)
//    }

//    fun switchVisibility(visible: Int) {
//        postHide()
//        visibility = visible
//        hideRunnable = kotlinx.coroutines.Runnable {
//            visibility = !visible
//        }
//        handler?.postDelayed(hideRunnable,3000)
//    }

    fun postHide(delay: Long) {
        Log.i("NAMTH","postHide delay="+delay)
        abortHide();
        Log.i("NAMTH","postHide delay 2222")
        mHideRunnable = kotlinx.coroutines.Runnable {
            Log.i("NAMTH","after 3s")
            hide();
        }
        Log.i("NAMTH","postHide delay 333mHideRunnable="+mHideRunnable)
        mHandler?.postDelayed(mHideRunnable, 3000)

    }

    private fun hide() {
        Log.i("NAMTH","hide isEnabled()="+isEnabled())
        if (!isEnabled() || !mVideoView.isPlaying) {
            return;
        }

        abortHide();
        Log.i("NAMTH","View.GONE")
        setVisibility(View.GONE);
    }

    private fun show(autoHideDelay: Long) {
        Log.i("NAMTH","show isEnabled="+isEnabled()+":autoHideDelay="+autoHideDelay)
        if (!isEnabled()) {
            return;
        }

        abortHide();

        setVisibility(View.VISIBLE);

        if (autoHideDelay > 0) {
            postHide(autoHideDelay);
        }
    }

    fun isShowing(): Boolean{
        return getVisibility() == View.VISIBLE;
    }

    fun switchShow() {
        Log.i("NAMTH","switchShow ="+isShowing())
        if (isShowing()) {
            hide()
        } else {
            show()
        }
    }

    fun show(autoHide: Boolean) {
        if (autoHide)
            show(INVISIBLE_DELAY)
        else
            show(0)
    }

    fun show() {
        show(true)
    }

    fun switchPlayButton(isPause: Boolean) {
        if (isPause)
            mBtnPause.setImageDrawable(resources.getDrawable(R.drawable.bc_pause_btn))
        else
            mBtnPause.setImageDrawable(resources.getDrawable(R.drawable.bc_play_btn))
    }


}

private fun SeekBar.setOnSeekBarChangeListener() {
}
