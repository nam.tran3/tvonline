package com.dn.kotlintv.screens.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dn.kotlintv.R
import com.dn.kotlintv.core.data.local.AppDatabase
import com.dn.kotlintv.core.data.local.model.Episode
import com.dn.kotlintv.core.data.local.model.EpisodeHistory
import com.dn.kotlintv.core.data.local.model.Program
import com.dn.kotlintv.utils.MessageEvent
import com.dn.kotlintv.utils.RequestApi
import com.dn.kotlintv.utils.Utils
import kotlinx.android.synthetic.main.fragment_home.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var mRCDailyRecommend: RecyclerView;
    private lateinit var mRCRecentlyPlayed: RecyclerView;
    private lateinit var mRCLastProgram: RecyclerView;
    private lateinit var mRCLastRelease: RecyclerView;
    private lateinit var rootView: View;
    private lateinit var mRecommendationAdapter: DailyAdapter;
    private lateinit var mRecentlyAdapter: RecentlyAdapter;
    private lateinit var mProgramAdapter: ProgramAdapter;
    private lateinit var mNewReleaseAdapter: LastestAdapter;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        Log.i("NAMTH", "onCreateView");
        rootView = inflater.inflate(R.layout.fragment_home, container, false);
        return rootView;
    }

    fun fetchWatching() {
        var db = context?.let { AppDatabase(it) }
        db?.getEpisodeHistoryDao()?.observerEpisodes()?.observe(viewLifecycleOwner, Observer {
            Log.i("NAMTH", "HOME fetchWatching=" + it.size)
            Utils.mRecentlyHistoryList = ArrayList(it)
            Log.i("NAMTH", "mRecentlyHistoryList=" + it.size)
            parseRecently(Utils.mAllEpisodeList)
            Log.i("NAMTH", "HOME Utils.mRecentlyList=" + Utils.mRecentlyList.size)
            recently_layout.visibility = if (!Utils.mRecentlyList.isEmpty()) View.VISIBLE else View.GONE
            mRecentlyAdapter.addItems(Utils.mRecentlyList)
            mRecentlyAdapter.notifyDataSetChanged()
        })
    }

    private fun initView() {
        initDailyRecycleView();
        initRecentlyPlayed();
        initLastProgram();
        initLastRelease();
    }

    private fun initRecentlyPlayed() {
        mRecentlyAdapter = RecentlyAdapter();
        mRCRecentlyPlayed = rootView.findViewById(R.id.rc_recently);
        mRCRecentlyPlayed.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
        mRCRecentlyPlayed.adapter = mRecentlyAdapter;
        var itemDecoration: DailyAdapter.ItemOffsetDecoration =
            DailyAdapter.ItemOffsetDecoration(10, 0)
        mRCRecentlyPlayed.addItemDecoration(itemDecoration)
    }

    private fun initLastRelease() {
        mNewReleaseAdapter = LastestAdapter();
        mNewReleaseAdapter.setHasStableIds(true)
        mRCLastRelease = rootView.findViewById(R.id.rc_new_release);
        mRCLastRelease.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
        mRCLastRelease.adapter = mNewReleaseAdapter;
        var itemDecoration: DailyAdapter.ItemOffsetDecoration =
            DailyAdapter.ItemOffsetDecoration(10, 0)
        mRCLastRelease.addItemDecoration(itemDecoration)
    }

    private fun initLastProgram() {
        mProgramAdapter = context?.let { ProgramAdapter(it) }!!;
        mRCLastProgram = rootView.findViewById(R.id.rc_last_program);
        mRCLastProgram.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
        mRCLastProgram.adapter = mProgramAdapter;
//        adapterEditorChoice.addItems(Utils.mEditorChoiceList);
        var itemDecoration: ProgramAdapter.ItemOffsetDecoration =
            ProgramAdapter.ItemOffsetDecoration(10, 0)
        mRCLastProgram.addItemDecoration(itemDecoration)
    }

    private fun initDailyRecycleView() {
        mRecommendationAdapter = DailyAdapter();
        mRCDailyRecommend = rootView.findViewById(R.id.rc_daily_recommendation);
        mRCDailyRecommend.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
        mRCDailyRecommend.adapter = mRecommendationAdapter;
        mRecommendationAdapter.addItems(Utils.mRecommendationList);
        var itemDecoration: DailyAdapter.ItemOffsetDecoration =
            DailyAdapter.ItemOffsetDecoration(10, 0)
        mRCDailyRecommend.addItemDecoration(itemDecoration)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fetchWatching()
        recommendation_layout.visibility = View.GONE
        recently_layout.visibility = View.GONE
        last_program_layout.visibility = View.GONE
        new_release_layout.visibility = View.GONE
        initView();
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment HomeFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            HomeFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onStart() {
        super.onStart()
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this)
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: MessageEvent) {
        if (event.message.equals("onDrawUI")) {
            Log.i("HIHI", "e=" + Utils.mAllEpisodeList.size)
            Log.i("HIHI", "pro=" + Utils.mProgramList.size)
            Log.i("HIHI", "e=" + Utils.mRecommendationList.size)
            Log.i("HIHI", "e=" + Utils.mEditorChoiceList.size)

            if (!Utils.mRecommendationList.isEmpty()) {
                mRecommendationAdapter.addItems(Utils.mRecommendationList)
                Log.i("NAMTH", "mRecommendationList=" + Utils.mRecommendationList.size)
                mRecommendationAdapter.notifyDataSetChanged()
                recommendation_layout.visibility = View.VISIBLE
            }

//            //Todo process recently late
            parseRecently(Utils.mAllEpisodeList)
            if (!Utils.mRecentlyList.isEmpty()) {
                recently_layout.visibility = View.VISIBLE
                Log.i("NAMTH", "mRecentlyList=" + Utils.mRecentlyList.size)
                mRecentlyAdapter.addItems(Utils.mRecentlyList)
                mRecentlyAdapter.notifyDataSetChanged()
            }

            if (!Utils.mProgramList.isEmpty()) {
                last_program_layout.visibility = View.VISIBLE
                val programList = ArrayList<Program>()
                val keys = Utils.mProgramList.keys.toTypedArray()
                Log.i("NAMTH", "keys=" + keys[0].toString())
                for (i in 0..(Utils.mProgramList.size - 1)) {
                    if (i < 10) {
                        Utils.mProgramList.get(keys[i])?.let { programList.add(it) }
                    }
                }
                mProgramAdapter.addItems(programList)
                mProgramAdapter.notifyDataSetChanged()
            }
            Log.i("NAMTH", "Utils.mAllEpisodeList=" + Utils.mAllEpisodeList.size)
            if (!Utils.mAllEpisodeList.isEmpty()) {
                new_release_layout.visibility = View.VISIBLE
                var sizeE = 0
                if (!Utils.mAllEpisodeList.isEmpty() && Utils.mAllEpisodeList?.size <= 10)
                    sizeE = Utils.mAllEpisodeList.size
                else
                    sizeE = 10

                var listLastest = Utils?.mAllEpisodeList?.subList(0, sizeE)
//                for (e in listLastest)
//                    Log.i("NAMTH", "Sub .mAllEpisodeList=" + e.title)
                mNewReleaseAdapter.addItems(listLastest)
                mNewReleaseAdapter.notifyDataSetChanged()
            }
            //Todo organization layout
//            recently_layout.y = recommendation_layout.y + recommendation_layout.layoutParams.height + 30
//            last_program_layout.y = recently_layout.y + recently_layout.layoutParams.height + 30
//            adapterRecently1.addItems(Utils.mProgramList)
//            adapterRecently1.notifyDataSetChanged()
            Log.i("NAMTH", "recommendation_layout.y=" + recently_layout.y)
//            recently_layout.y = recommendation_layout.y
//
        }
    }

    private fun parseRecently(listAll: ArrayList<Episode>) {
        Utils.mRecentlyList.clear()
        Utils.mRecentlyList = ArrayList()
        if (!Utils.mRecentlyHistoryList.isEmpty()) {
            for (his in Utils.mRecentlyHistoryList) {
                val videoId = his.video_id
                for (e in listAll) {
                    if (videoId.equals(e.videoId)) {
                        Utils.mRecentlyList.add(e)
                    }
                }
            }
        }
        Log.i("NAMTH", "parseRecently=" + Utils.mRecentlyList.size)
    }

    fun notifyHome() {
        mProgramAdapter.notifyDataSetChanged()
    }
}
