package com.dn.kotlintv.core.data.response

import com.google.gson.annotations.SerializedName

data class GenreResponse<T>(
    @SerializedName("genre") val genreList: T
)