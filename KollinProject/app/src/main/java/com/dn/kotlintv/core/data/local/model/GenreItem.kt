package com.dn.kotlintv.core.data.local.model

import com.google.gson.annotations.SerializedName

data class GenreItem (
    @SerializedName("genre_name") val genreName: String,
    @SerializedName("key_genre") val genrePosition: Int
)