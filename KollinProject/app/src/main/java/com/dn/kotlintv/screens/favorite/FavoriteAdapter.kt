package com.dn.kotlintv.screens.favorite

import android.graphics.Rect
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.dn.kotlintv.R
import com.dn.kotlintv.core.data.local.model.Program
import com.dn.kotlintv.databinding.FavoriteItemLayoutBinding
import kotlinx.android.synthetic.main.favorite_item_layout.view.*

class FavoriteAdapter : RecyclerView.Adapter<FavoriteAdapter.ViewHolder>() {
    private var list = ArrayList<Program>()
    private var mBinding: FavoriteItemLayoutBinding? = null
    private var editView = false

    companion object {
        private var currentPosition: Int = 0


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        mBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.favorite_item_layout,
            parent,
            false
        ) as FavoriteItemLayoutBinding
        return ViewHolder(mBinding!!);
    }

    override fun getItemCount(): Int {
        return list!!.size;
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(list.get(position))

    fun addItems(items: ArrayList<Program>) {
        this.list.clear()
        this.list?.addAll(items)
    }

    inner class ViewHolder(val sbinding: FavoriteItemLayoutBinding) :
        RecyclerView.ViewHolder(sbinding.root),
        View.OnClickListener {
        fun bind(item: Program) {
            sbinding.program = item
            if (editView) {
                sbinding.root.item_checkbox.visibility = View.VISIBLE
            } else {
                sbinding.root.item_checkbox.visibility = View.GONE
            }
            if (item.isCheck)
                (sbinding.root.item_checkbox as ImageView).setImageDrawable(
                    sbinding.root.context.getDrawable(
                        R.drawable.list_check_on
                    )
                )
            else
                (sbinding.root.item_checkbox as ImageView).setImageDrawable(
                    sbinding.root.context.getDrawable(
                        R.drawable.list_ckeck_off
                    )
                )
        }

        init {
            var view = sbinding.root as View
            view.setOnClickListener(this)
            view.item_checkbox.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            if (v?.id == R.id.item_checkbox) {
                list.get(adapterPosition).isCheck = !list.get(adapterPosition).isCheck
                Log.i(
                    "NAMTH",
                    "onClick=" + adapterPosition + "value=" + list.get(adapterPosition).isCheck
                )
                if (list.get(adapterPosition).isCheck)
                    (v as ImageView).setImageDrawable(v.context.getDrawable(R.drawable.list_check_on))
                else
                    (v as ImageView).setImageDrawable(v.context.getDrawable(R.drawable.list_ckeck_off))
            }
        }
    }

    class ItemOffsetDecoration(private val horizontalSpace: Int, private val verticalSpace: Int) :
        RecyclerView.ItemDecoration() {

        override public fun getItemOffsets(
            outRect: Rect,
            view: View,
            parent: RecyclerView,
            state: RecyclerView.State
        ) {
            super.getItemOffsets(outRect, view, parent, state)
            outRect.set(horizontalSpace, 0, horizontalSpace, 0)
        }
    }

    fun setVIEW_TYPE(viewType: Boolean) {
        editView = viewType
        if (!editView)
            setDataChangeAll(editView)
        notifyDataSetChanged()
    }

    fun setSelectAll(isSelectAll : Boolean) {
        editView = true
        setDataChangeAll(isSelectAll)
        notifyDataSetChanged()
    }

    private fun setDataChangeAll(value: Boolean) {
        for (fa in list)
            fa.isCheck = value
    }
}