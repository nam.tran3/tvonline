package com.dn.kotlintv.core.data.local.model

import com.google.gson.annotations.SerializedName

data class Program (
    @SerializedName("programId") val programId : String,
    @SerializedName("title") val title : String,
    @SerializedName("description") val description : String,
    @SerializedName("logo") val logo : String,
    @SerializedName("genre") val genre : String,
    @SerializedName("episodes") val episodes : ArrayList<Episode>,
    @SerializedName("CreateBy") val createBy : String,
    var isFavorite: Boolean,
    var isCheck: Boolean = false


) : Cloneable  {
    override public fun clone(): Any {
        return super.clone()
    }
}