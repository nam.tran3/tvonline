package com.dn.kotlintv.core.di.module

import android.util.Log
import com.google.gson.GsonBuilder
import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

class ApiClient {
//    lateinit var retrofit:Retrofit
//    init {
//        retrofit = retrofit(okHttpClient(httpLoggingInterceptor()), gson())
//    }
//
//    fun httpLoggingInterceptor(): HttpLoggingInterceptor {
//        val interceptor = HttpLoggingInterceptor()
//        interceptor.level = HttpLoggingInterceptor.Level.BASIC
//        return interceptor
//    }
//
//    fun okHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
//        val builder = OkHttpClient.Builder()
//        return builder
//            .addInterceptor(httpLoggingInterceptor)
//            .build()
//    }
//
//    fun gson(): Gson {
//        val gsonBuilder = GsonBuilder()
//        gsonBuilder.registerTypeAdapter(DateTime::class.java, DateTimeConverter())
//        return gsonBuilder.create()
//    }
//
//    fun retrofit(okHttpClient: OkHttpClient, gson: Gson): Retrofit {
//        return Retrofit.Builder()
//            .addConverterFactory(GsonConverterFactory.create(gson))
//            .addCallAdapterFactory(CoroutineCallAdapterFactory())
////            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//            .client(okHttpClient)
//            .baseUrl("https://gitlab.com/nam.tran3/tvonline/snippets/")
//            .build()
//    }
//
//    fun getService() : TVService {
//        return retrofit.create<TVService>(TVService::class.java)
//    }

    private val okHttpClient by lazy { OkHttpClient() }

    private val retrofit: Retrofit by lazy {
        Log.e("AppClient", "Creating Retrofit Client")
        val builder = Retrofit.Builder()
            .baseUrl("https://gitlab.com/nam.tran3/tvonline/snippets/")
            .client(okHttpClient)
//            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
        val dispatcher = Dispatcher()
        dispatcher.maxRequests = 1

        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val client: OkHttpClient = okHttpClient.newBuilder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .addInterceptor(loggingInterceptor)
            .dispatcher(dispatcher)
            .build()
        builder.client(client).build()
    }

    fun <T> createService(tClass: Class<T>?): T {
        return retrofit.create(tClass)
    }
}