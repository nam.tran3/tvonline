package com.dn.kotlintv.screens.program

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.size
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.dn.kotlintv.R
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import kotlinx.android.synthetic.main.fragment_program.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ProgramFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ProgramFragment(context: Context) : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var mContext: Context? = null

    init {
        mContext = context
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_program, container, false)
//        view.tabLayout.addTab(view.tabLayout.newTab().setText("All"))
//        view.tabLayout.addTab(view.tabLayout.newTab().setText("Science"))
//        view.tabLayout.addTab(view.tabLayout.newTab().setText("Humor"))
//        view.tabLayout.addTab(view.tabLayout.newTab().setText("Discovery"))
//        view.tabLayout.addTab(view.tabLayout.newTab().setText("Emotional"))
//        view.tabLayout.addTab(view.tabLayout.newTab().setText("Sport"))
//        view.tabLayout.tabGravity = TabLayout.GRAVITY_FILL

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val viewpager = view.findViewById<View>(R.id.view_paper_program) as ViewPager
        val adapter = mContext?.let { ProgramPagerAdapter(it, childFragmentManager) }
        viewpager.adapter = adapter
        val tabLayouts = view.findViewById<View>(R.id.tabLayout) as TabLayout
        tabLayouts.setupWithViewPager(viewpager)
        var countTab = tabLayouts.tabCount;

        for (pos in 0..(countTab-1)) {
            tabLayouts.getTabAt(pos)?.setCustomView(adapter?.getTabView(pos))
        }

//        view.tabLayout.setupWithViewPager(view.view_paper_program)
        view.tabLayout.setOnTabSelectedListener(object : OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewpager.setCurrentItem(tab.position)
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ProgramFragment.
         */

    }
}
