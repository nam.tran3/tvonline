package com.dn.kotlintv

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.dn.kotlintv.core.data.repository.TVRepository
import com.dn.kotlintv.screens.favorite.FavoriteFagment
import com.dn.kotlintv.screens.home.HomeFragment
import com.dn.kotlintv.screens.program.ProgramFragment
import com.dn.kotlintv.screens.watching.WatchingFragment
import com.dn.kotlintv.utils.MessageEvent
import com.dn.kotlintv.utils.RequestApi
import com.dn.kotlintv.utils.ViewModelProviderFactory
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.navigate_bottom_layout.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class MainActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var mToolbar: Toolbar;
    private lateinit var mCurrentFragment: Fragment;
    var mHomeFragment: HomeFragment? = null
    var mProgramFragment: ProgramFragment? = null;
    var mFavoriteFagment: FavoriteFagment? = null;
    var mWatchingFragment: WatchingFragment? = null;

    lateinit var mMainViewModel: MainViewModel
    lateinit var mProviderFactory: ViewModelProviderFactory
    var mFinishTimelineRequest: Boolean = false
    var mFinishRecommendRequest: Boolean = false
    var mTitleBar: TextView? = null
    var mEditBar: TextView? = null
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
        setContentView(R.layout.activity_main)
        setupActionBar();
        mHomeFragment = HomeFragment();
        supportFragmentManager.beginTransaction().replace(R.id.main_fragment, mHomeFragment!!)
            .commit();
        txtHome.setTextColor(getColor(R.color.txt_nav_home_select))
        mCurrentFragment = mHomeFragment!!;
        val img: ImageView? = findViewById(R.id.image);
        img?.clipToOutline = false;
        mMainViewModel = ViewModelProviders.of(this, ViewModelProviderFactory(TVRepository()))
            .get(MainViewModel::class.java)

        showLoading(true)
        initView()
        RequestApi(this, mMainViewModel)
        loadDataServer()
    }

    private fun initView() {

        home_layout.setOnClickListener(this)
        program_layout.setOnClickListener(this)
        favorite_layout.setOnClickListener(this)
        watching_layout.setOnClickListener(this)
        ranking_layout.setOnClickListener(this)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onClick(v: View?) {
       when(v?.id) {
           R.id.home_layout -> {
               if (mHomeFragment == null) {
                   mHomeFragment = HomeFragment()
               }
               hideShowFragment(mCurrentFragment, mHomeFragment!!)

               imgHome.setImageDrawable(getDrawable(R.drawable.ic_tab_home_selected))
               txtHome.setTextColor(getColor(R.color.txt_nav_home_select))

               imgProgram.setImageDrawable(getDrawable(R.drawable.ic_tab_list_unselected))
               txtProgram.setTextColor(getColor(R.color.txt_nav_home_unselect))

               imgFavoriteNav.setImageDrawable(getDrawable(R.drawable.ic_tab_favorite_unselected))
               txtFavorite.setTextColor(getColor(R.color.txt_nav_home_unselect))

               imgWatching.setImageDrawable(getDrawable(R.drawable.ic_tab_watching_unselected))
               txtWatching.setTextColor(getColor(R.color.txt_nav_home_unselect))

               imgRanking.setImageDrawable(getDrawable(R.drawable.ic_tab_ranking_unselected))
               txtRanking.setTextColor(getColor(R.color.txt_nav_home_unselect))

               mTitleBar?.text = getString(R.string.txt_home);
               mEditBar?.visibility = View.GONE
               mHomeFragment?.notifyHome()
           }
           R.id.program_layout -> {
                if (mProgramFragment == null) {
                    mProgramFragment = ProgramFragment(this)
                }
               hideShowFragment(mCurrentFragment, mProgramFragment!!)

               imgHome.setImageDrawable(getDrawable(R.drawable.ic_tab_home_unselected))
               txtHome.setTextColor(getColor(R.color.txt_nav_home_unselect))

               imgProgram.setImageDrawable(getDrawable(R.drawable.ic_tab_list_selected))
               txtProgram.setTextColor(getColor(R.color.txt_nav_home_select))

               imgFavoriteNav.setImageDrawable(getDrawable(R.drawable.ic_tab_favorite_unselected))
               txtFavorite.setTextColor(getColor(R.color.txt_nav_home_unselect))

               imgWatching.setImageDrawable(getDrawable(R.drawable.ic_tab_watching_unselected))
               txtWatching.setTextColor(getColor(R.color.txt_nav_home_unselect))

               imgRanking.setImageDrawable(getDrawable(R.drawable.ic_tab_ranking_unselected))
               txtRanking.setTextColor(getColor(R.color.txt_nav_home_unselect))

               mTitleBar?.text = getString(R.string.txt_program);
               mEditBar?.visibility = View.GONE
           }
           R.id.favorite_layout -> {

               if (mFavoriteFagment == null) {
                   mFavoriteFagment = FavoriteFagment()
               }
               hideShowFragment(mCurrentFragment, mFavoriteFagment!!)

               imgHome.setImageDrawable(getDrawable(R.drawable.ic_tab_home_unselected))
               txtHome.setTextColor(getColor(R.color.txt_nav_home_unselect))

               imgProgram.setImageDrawable(getDrawable(R.drawable.ic_tab_list_unselected))
               txtProgram.setTextColor(getColor(R.color.txt_nav_home_unselect))

               imgFavoriteNav.setImageDrawable(getDrawable(R.drawable.ic_tab_favorite_selected))
               txtFavorite.setTextColor(getColor(R.color.txt_nav_home_select))

               imgWatching.setImageDrawable(getDrawable(R.drawable.ic_tab_watching_unselected))
               txtWatching.setTextColor(getColor(R.color.txt_nav_home_unselect))

               imgRanking.setImageDrawable(getDrawable(R.drawable.ic_tab_ranking_unselected))
               txtRanking.setTextColor(getColor(R.color.txt_nav_home_unselect))
               mTitleBar?.text = getString(R.string.txt_favorite);
               mEditBar?.visibility = View.VISIBLE
               (mCurrentFragment as FavoriteFagment)?.setViewType(false)
           }
           R.id.watching_layout -> {

               if (mWatchingFragment == null) {
                   mWatchingFragment = WatchingFragment()
               }
               hideShowFragment(mCurrentFragment, mWatchingFragment!!)

               imgHome.setImageDrawable(getDrawable(R.drawable.ic_tab_home_unselected))
               txtHome.setTextColor(getColor(R.color.txt_nav_home_unselect))

               imgProgram.setImageDrawable(getDrawable(R.drawable.ic_tab_list_unselected))
               txtProgram.setTextColor(getColor(R.color.txt_nav_home_unselect))

               imgFavoriteNav.setImageDrawable(getDrawable(R.drawable.ic_tab_favorite_unselected))
               txtFavorite.setTextColor(getColor(R.color.txt_nav_home_unselect))

               imgWatching.setImageDrawable(getDrawable(R.drawable.ic_tab_watching_selected))
               txtWatching.setTextColor(getColor(R.color.txt_nav_home_select))

               imgRanking.setImageDrawable(getDrawable(R.drawable.ic_tab_ranking_unselected))
               txtRanking.setTextColor(getColor(R.color.txt_nav_home_unselect))
               mTitleBar?.text = getString(R.string.txt_watching);
               mEditBar?.visibility = View.VISIBLE
               (mCurrentFragment as WatchingFragment)?.setViewType(false)
           }
           R.id.ranking_layout -> {
               imgHome.setImageDrawable(getDrawable(R.drawable.ic_tab_home_unselected))
               txtHome.setTextColor(getColor(R.color.txt_nav_home_unselect))

               imgProgram.setImageDrawable(getDrawable(R.drawable.ic_tab_list_unselected))
               txtProgram.setTextColor(getColor(R.color.txt_nav_home_unselect))

               imgFavoriteNav.setImageDrawable(getDrawable(R.drawable.ic_tab_favorite_unselected))
               txtFavorite.setTextColor(getColor(R.color.txt_nav_home_unselect))

               imgWatching.setImageDrawable(getDrawable(R.drawable.ic_tab_watching_unselected))
               txtWatching.setTextColor(getColor(R.color.txt_nav_home_unselect))

               imgRanking.setImageDrawable(getDrawable(R.drawable.ic_tab_ranking_selected))
               txtRanking.setTextColor(getColor(R.color.txt_nav_home_select))
               mTitleBar?.text = getString(R.string.txt_ranking);
               mEditBar?.visibility = View.GONE
           }
           R.id.txt_edit -> {
               if (mCurrentFragment is FavoriteFagment) {
                   (mCurrentFragment as FavoriteFagment)?.setViewType(true)
               } else if (mCurrentFragment is WatchingFragment) {
                   (mCurrentFragment as WatchingFragment)?.setViewType(true)
               }
           }
       }
    }

    private fun setupActionBar() {
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        val actionBar: ActionBar? = getSupportActionBar();
        actionBar?.setDisplayShowTitleEnabled(true);
        actionBar?.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar?.setCustomView(R.layout.actionbar_custom);
        actionBar?.setDisplayShowTitleEnabled(false);
        mTitleBar = actionBar?.customView?.findViewById(R.id.txt_title);
        mTitleBar?.text = getString(R.string.txt_home);
        mEditBar = actionBar?.customView?.findViewById(R.id.txt_edit);
        mEditBar?.visibility = View.GONE
        mEditBar?.setOnClickListener(this)
    }

    fun loadDataServer() {
        RequestApi.fetchTimeline();
        RequestApi.fetchRecommendation();
        RequestApi.fetchEditorChoice();
        RequestApi.fetchGenre();
        RequestApi.fetchFavorite();
        RequestApi.fetchWatching();

    }
    fun hideShowFragment(
        hide: Fragment?,
        show: Fragment
    ) {
        mCurrentFragment = show

        if (hide == null) {
            supportFragmentManager.beginTransaction().show(show).commit()
            return
        }
        if (!show.isAdded) {
            supportFragmentManager.beginTransaction().add(R.id.main_fragment, show)
                .hide(hide).show(show).commit()
        } else {
            supportFragmentManager.beginTransaction().hide(hide).show(show).commit()
        }
    }

    private fun showLoading(ishow: Boolean) {
        if (ishow) {
            getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            loadingBar.visibility = View.VISIBLE
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            loadingBar.visibility = View.GONE
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: MessageEvent) {
        if (event.message.equals("finishLoading")) {
            showLoading(false)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this)
        }
    }

}
