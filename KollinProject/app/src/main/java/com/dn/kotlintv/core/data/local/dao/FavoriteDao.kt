package com.dn.kotlintv.core.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.dn.kotlintv.core.data.local.model.Favorite
import retrofit2.http.DELETE

@Dao
interface FavoriteDao {
    @Query("Select * from FAVORITE")
    fun getAll(): List<Favorite>

    @Query("Select * from FAVORITE order by createDate DESC")
    fun observerFavorites(): LiveData<List<Favorite>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFavorite(item: Favorite)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFavoriteList(list: List<Favorite>)

    @Query("Delete from FAVORITE where programId = :id")
    fun deleteById(id: String)

    @Delete
    fun delete(favorite: Favorite)

    @Query("Delete from FAVORITE")
    fun deleteAll()
}