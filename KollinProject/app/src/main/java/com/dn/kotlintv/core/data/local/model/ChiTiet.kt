package com.dn.kotlintv.core.data.local.model

import com.google.gson.annotations.SerializedName

data class ChiTiet(@SerializedName("title") val title: String, @SerializedName("desc") val desc: String,
                   @SerializedName("thumbnail") val thumbnail: String,@SerializedName("ingredients") val ingredients: List<String>,
                   @SerializedName("tutorials") val tutorials: List<String>)