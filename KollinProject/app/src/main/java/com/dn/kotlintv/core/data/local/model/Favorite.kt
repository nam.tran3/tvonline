package com.dn.kotlintv.core.data.local.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import java.sql.Date

@Entity(tableName = "favorite")
data class Favorite(@PrimaryKey @ColumnInfo(name = "programId") val program_id: String, @ColumnInfo(name = "createDate") val create_date: Long)