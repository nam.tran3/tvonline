package com.dn.kotlintv.screens.favorite

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.dn.kotlintv.R
import com.dn.kotlintv.core.data.local.AppDatabase
import com.dn.kotlintv.core.data.local.model.Favorite
import com.dn.kotlintv.core.data.local.model.Program
import com.dn.kotlintv.utils.Utils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.sql.Date

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FavoriteFagment.newInstance] factory method to
 * create an instance of this fragment.
 */
class FavoriteFagment() : Fragment(), View.OnClickListener {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var mRCFavorite: RecyclerView? = null
    private var mFavoriteAdapter: FavoriteAdapter? = null
    var db: AppDatabase? = null
    private var mFavoriteList = ArrayList<Program>()
    private var mFavoriteSaveList = ArrayList<Favorite>()
    private var mEditLayout: RelativeLayout? = null
    private var mSelectAll: TextView? = null
    private var mDelete: TextView? = null
    private var mIsSelectAll = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        db = getContext()?.let { AppDatabase(it) }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_favorite, container, false)

        return view
    }

    override fun onViewCreated(rootView: View, savedInstanceState: Bundle?) {
        super.onViewCreated(rootView, savedInstanceState)
        mRCFavorite = rootView.findViewById(R.id.rcFavorite)
        mEditLayout = rootView.findViewById(R.id.edit_layout)
        mSelectAll = rootView.findViewById(R.id.select_all)
        mSelectAll?.setOnClickListener(this)
        mDelete = rootView.findViewById(R.id.delete)
        mDelete?.setOnClickListener(this)
        mFavoriteAdapter = FavoriteAdapter()
        mRCFavorite?.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
         mRCFavorite?.adapter = mFavoriteAdapter

        viewLifecycleOwner?.let { db?.getFavoriteDao()?.observerFavorites()?.observe(it, Observer {
            mFavoriteList = ArrayList<Program>()
            if (!it.isEmpty()) {
                mFavoriteSaveList = ArrayList(it)

                for (fa in mFavoriteSaveList) {
                    val id = fa.program_id
                    if (Utils.mProgramList.containsKey(id)) {
                        Utils.mProgramList.get(id)?.let { it1 -> mFavoriteList.add(it1) }
                    }
                }
                for (f in mFavoriteList)
                    Log.i("NAMTH","favorite save g="+f)

            }
            mFavoriteAdapter?.addItems(mFavoriteList)
            mFavoriteAdapter?.notifyDataSetChanged()
        }) };
    }

    fun setViewType(type: Boolean) {
        mEditLayout?.visibility = View.GONE
        if (type) {
            mEditLayout?.visibility = View.VISIBLE
        }
        mFavoriteAdapter?.setVIEW_TYPE(type)
    }

    companion object {

    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.select_all) {
            mIsSelectAll = !mIsSelectAll
            mSelectAll?.text = if (mIsSelectAll) getString(R.string.txt_unselect_all) else getString(R.string.txt_select_all)
            mFavoriteAdapter?.setSelectAll(mIsSelectAll)
        } else if (v?.id == R.id.delete) {
            var count = 0
            var listUpdate = ArrayList(mFavoriteSaveList)
            for (fa in mFavoriteList) {
                Log.i("NAMTH","isCheck="+mFavoriteSaveList.size)

                if (fa.isCheck) {

                    var proId = fa.programId
                    for (faSave in listUpdate) {
                        Log.i("NAMTH","faSave="+faSave.program_id)
                        if (proId.equals(faSave.program_id))
                        {
                            Log.i("NAMTH","remove nef")
                            listUpdate.remove(faSave)
                            count++
                            break
                        }
                    }
                    fa.isFavorite = false
                }
            }

            Log.i("NAMTH","listUpdate="+listUpdate.size+":count="+count)
            GlobalScope.launch {
                withContext(Dispatchers.IO) {

                    if (count > 0 ) {
                        db?.getFavoriteDao()?.deleteAll()
                        if (!listUpdate.isEmpty()) {
                            db?.getFavoriteDao()?.insertFavoriteList(listUpdate)
                        }
                    }

                }

            }
        }
    }
}
