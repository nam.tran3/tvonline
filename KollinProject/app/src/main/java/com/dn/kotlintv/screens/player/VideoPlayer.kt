package com.dn.kotlintv.screens.player

import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.TextView
import android.widget.VideoView
import androidx.appcompat.app.AppCompatActivity
import com.dn.kotlintv.R
import com.dn.kotlintv.core.data.local.AppDatabase
import com.dn.kotlintv.core.data.local.model.Episode
import com.dn.kotlintv.core.data.local.model.EpisodeHistory
import com.dn.kotlintv.utils.Utils
import kotlinx.coroutines.*
import java.lang.Exception
import java.util.*


class VideoPlayer : AppCompatActivity(), View.OnClickListener, View.OnTouchListener{
    lateinit var mMediaPlayer: MediaPlayer
    lateinit var mVideoview: VideoView
    lateinit var mSeekbar: SeekBar
    lateinit var mCurrentTime: TextView
    lateinit var mDurationTime: TextView
    var mIsShowControl: Boolean = false
    private val job = SupervisorJob()
    private val scope = CoroutineScope(Dispatchers.Default + job)
    var mPosition: Int = 0
    lateinit var handler: Handler
    var hideRunnable: Runnable? = null
    lateinit var mVideoController: VideoController
    private var db: AppDatabase? = null
    private var mEpsideoId: String = ""
    private var mEpisodesObj: EpisodeHistory? = null
    private var mEpisode: Episode? = null
    private var mTxtTitle: TextView? = null
    private var mTxtEndDate: TextView? = null
    private var mTxtDecs: TextView? = null
    private var mBtnBack: ImageView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_video_player)
        initView()
        updateView()

    }

    private fun initView() {
        mTxtTitle = findViewById(R.id.episode_title)
        mTxtEndDate = findViewById(R.id.end_date)
        mTxtDecs = findViewById(R.id.desc_episode)
        mVideoview = findViewById(R.id.video_view) as VideoView
        mVideoController = findViewById(R.id.video_controller) as VideoController
        mBtnBack = findViewById(R.id.btn_back)
        mBtnBack?.setOnClickListener(this)
        mVideoview.setOnTouchListener(this)
        val uri: Uri =
//                Uri.parse("http://vfx.mtime.cn/Video/2019/02/08/mp4/190208204943376259.mp4")
//                Uri.parse("http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4")
//                Uri.parse("https://raw.githubusercontent.com/o7planning/webexamples/master/_testdatas_/mov_bbb.mp4")
            Uri.parse("https://hlsak-a.akamaihd.net/3636334163001/3636334163001_5566790474001_5566768721001.m3u8?pubId=3636334163001&videoId=5566768721001")
        mVideoview.setVideoURI(uri)
        mVideoview.setOnPreparedListener(MediaPlayer.OnPreparedListener {
            Log.i("NAMTH", "onPrepared")
//            it.setOnVideoSizeChangedListener(MediaPlayer.OnVideoSizeChangedListener { mediaPlayer, w, h ->  })

            mVideoController.setDuration(it.duration)
            timeCounter()
            mVideoview.start()
            mEpisodesObj?.let { mVideoview.seekTo(it?.position.toInt()) }

        })

        mVideoview.setOnCompletionListener { mp -> onFinish()}
        handler = Handler()

        mVideoController.visibility = View.INVISIBLE
        mVideoController.setVideoView(mVideoview)
    }

    private fun onFinish(){
        mVideoController.setProgress(mVideoview.duration)
        mVideoController.setCurrentTime(getTimeVideo(mVideoview.duration))
        mVideoController.setDurationTime(getTimeVideo(mVideoview.duration))
        mVideoController.switchPlayButton(false)
    }

    private fun updateView() {
        mEpsideoId = getIntent().getStringExtra("episodeId") as String;
        Log.i("NAMTH","mEpsideoId="+mEpsideoId)
        for (e in Utils.mAllEpisodeList) {
            if (e.videoId.equals(mEpsideoId)) {
                mEpisode = e
            }
        }

        db = AppDatabase(this)
        GlobalScope.launch {
            withContext(Dispatchers.IO) {
                mEpisodesObj = db?.getEpisodeHistoryDao()?.getEpisodeById(mEpsideoId)

            }
        }

        mTxtTitle?.text = mEpisode?.title
        mTxtEndDate?.text = mEpisode?.end_date
        mTxtDecs?.text = mEpisode?.description
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_back -> {
                onBackPressed()
            }

        }
    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        when(event?.action) {
            MotionEvent.ACTION_DOWN -> {
//                postHide()
//                mVideoController.visibility = View.VISIBLE
//                hideRunnable = kotlinx.coroutines.Runnable {
//                    mVideoController.visibility = View.INVISIBLE
//                }
//                handler?.postDelayed(hideRunnable,3000)
//                mVideoController?.abortHide()
                if (mVideoview.currentPosition == mVideoview.duration) {

                }
                mVideoController?.show(true)
            }
            MotionEvent.ACTION_UP -> {

            }
        }
        return v?.onTouchEvent(event) ?: true
    }

    private fun postHide() {
        if (hideRunnable != null)
            handler.removeCallbacks(hideRunnable)
    }

    private fun getTimeVideo(time: Int): String {
        var duration: Int = time / 1000
        var hours = duration / 3600
        var minutes = duration / 60 - hours * 60
        var seconds = duration - hours * 3600 - minutes * 60
        var durationTime: String = ""
        if (hours == 0)
            durationTime = (String.format("%02d:%02d", minutes, seconds))
        else
            durationTime = (String.format("%d:%02d:%02d", hours, minutes, seconds))
        return durationTime
    }

    private fun timeCounter() {
        val timer: Job = startCoroutineTimer(delayMillis = 0, repeatMillis = 1000) {
            updateUI()
        }

        timer.start()
    }

    private fun updateUI() {
        if (mVideoview.isPlaying) {
            mVideoController.setProgress(mVideoview.currentPosition)
            mVideoController.setCurrentTime(getTimeVideo(mVideoview.currentPosition))
            mVideoController.setDurationTime(getTimeVideo(mVideoview.duration))
        }
    }

    private fun startCoroutineTimer(delayMillis: Long = 0, repeatMillis: Long = 0, action: () -> Unit) = scope.launch(Dispatchers.Main) {
        delay(delayMillis)
        if (repeatMillis > 0) {
            while (true) {
                action()
                delay(repeatMillis)
            }
        } else {
            action()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i("NAMTH","ondestroy video")


    }

    override fun onBackPressed() {
        super.onBackPressed()
        Log.i("NAMTH","onBackPressed")
        GlobalScope.launch {
            withContext(Dispatchers.IO) {
                try {
                    if (mEpisodesObj != null) {
                        if (mVideoview?.currentPosition == mVideoview.duration || mVideoview?.currentPosition == 0) {
                            mEpisodesObj?.video_id?.let { db?.getEpisodeHistoryDao()?.deleteById(it) }
                            return@withContext
                        }
                        mEpisodesObj?.create_date = Date().time
                        mEpisodesObj?.position =  mVideoview?.currentPosition.toLong()
                    } else {
                        mEpisodesObj = EpisodeHistory(mEpsideoId,  mVideoview?.currentPosition.toLong(), Date().time)
                    }
                    mEpisodesObj?.let { db?.getEpisodeHistoryDao()?.insertEpisode(it) }
                }catch (e:Exception) {
                    Log.i("NAMTH","sql err="+e.toString())
                }

            }

        }
    }
}

