package com.dn.kotlintv.core.data.local.model

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.google.gson.annotations.SerializedName
import kotlinx.android.synthetic.main.episode_layout.view.*

data class Episode (

    @SerializedName("videoId") val videoId : String,
    @SerializedName("title") val title : String,
    @SerializedName("description") val description : String,
    @SerializedName("videoUrl") val videoUrl : String,
    @SerializedName("start_date") val start_date : String,
    @SerializedName("end_date") val end_date : String,
    @SerializedName("thumbnail") val thumbnail : String,
    var createBy : String,
    var isCheck: Boolean
)