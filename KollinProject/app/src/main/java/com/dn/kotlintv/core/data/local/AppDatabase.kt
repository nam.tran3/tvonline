package com.dn.kotlintv.core.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.dn.kotlintv.core.data.local.dao.EpisodeHistoryDao
import com.dn.kotlintv.core.data.local.dao.FavoriteDao
import com.dn.kotlintv.core.data.local.model.EpisodeHistory
import com.dn.kotlintv.core.data.local.model.Favorite

@Database(entities = [Favorite::class, EpisodeHistory::class], version = 5)
abstract class AppDatabase: RoomDatabase() {
    abstract fun getFavoriteDao(): FavoriteDao
    abstract fun getEpisodeHistoryDao(): EpisodeHistoryDao

    companion object {
        private var instance: AppDatabase? = null
        private var databaseName = "tvOnline.db"
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also { instance = it }
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(context, AppDatabase::class.java, databaseName).build()

    }
}