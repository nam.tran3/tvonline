package com.dn.kotlintv.utils

//class Resource<T>(val status: Status, val data: T? = null, val err: Throwable? = null) {
class   Resource<out T>(val status: Status, val data: T?, val message: String?) {
    companion object {
//        fun <T> success(data: T?) = Resource(Status.SUCCESS, data)
//        fun <T> error(err: Throwable?) = Resource<T>(Status.ERROR, null, err)
//        fun <T> loading() = Resource<T>(Status.LOADING)

        fun <T> success(data: T): Resource<T> = Resource(status = Status.SUCCESS, data = data, message = null)

        fun <T> error(data: T?, message: String): Resource<T> =
            Resource(status = Status.ERROR, data = data, message = message)

        fun <T> loading(data: T?): Resource<T> = Resource(status = Status.LOADING, data = data, message = null)
    }

    enum class Status {
        SUCCESS, ERROR, LOADING
    }
}