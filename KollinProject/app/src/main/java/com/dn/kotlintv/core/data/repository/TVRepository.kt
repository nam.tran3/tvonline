package com.dn.kotlintv.core.data.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import com.dn.kotlintv.core.TVService
import com.dn.kotlintv.core.data.local.model.Program
import com.dn.kotlintv.core.data.response.RecommendResponse
import com.dn.kotlintv.core.data.response.TimelineResponse
import com.dn.kotlintv.core.di.module.ApiClient
import com.dn.kotlintv.utils.Resource
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers

class TVRepository() {
    var mTVService: TVService
    var mApiClient: ApiClient
    var programList = MutableLiveData<Resource<TimelineResponse<List<Program>>>>()
    init {
        mApiClient = ApiClient()
        mTVService = mApiClient.createService(TVService::class.java)
    }

//    fun getTimeLine(): Deferred<TimelineResponse> {
//        if (mTVService == null)
//            Log.i("NAMTH","getTimeLine null")
//        Log.i("NAMTH","getTimeLine call service")
//        return mTVService.getTimeLine()
//    }
//
//    fun getRecommendation(): Single<RecommendResponse> {
//        return mTVService.getRecommendation()
//    }
//    fun requestTimeline1(): Deferred<TimelineResponse<List<Program>>> = async  {
//        emit(Resource.Status.LOADING)
//        var response = mTVService.requestTimeline()
//        if(response.isCompleted) {
//            emit(Resource.success(response))
//        }
//    }
//    suspend fun requestTimeline() {
//    var result = mTVService.requestTimeline()
//    when (result.)
//}

    suspend fun requestTimeline() = mTVService.requestTimeline()
    suspend fun getRecommendation() = mTVService.getRecommendation()
    suspend fun getEditorChoice() = mTVService.getEditorChoice()
    suspend fun getGenre() = mTVService.getGenre()
}