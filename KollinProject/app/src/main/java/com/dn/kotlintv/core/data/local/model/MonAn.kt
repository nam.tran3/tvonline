package com.dn.kotlintv.core.data.local.model

import com.google.gson.annotations.SerializedName

data class MonAn(@SerializedName("MonAns") val monan: ArrayList<ChiTiet>)