package com.dn.kotlintv.utils

import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.episode_layout.view.*

@BindingAdapter("app:imageUrl")
fun loadImage(view: ImageView, thumbnail: String) {
    Glide
        .with(view.getContext())
        .load(thumbnail)
        .thumbnail(
            Glide
                .with(view.getContext())
                .load(thumbnail)
        )
        .into(view)
}

@BindingAdapter("app:visibility")
fun setvisibility(view: View, isShow: Boolean) {
    Log.i("NAMTH","isShow="+isShow)
    view.visibility =    if (isShow) View.VISIBLE else View.GONE
}
