package com.dn.kotlintv.utils

import android.util.Log
import com.dn.kotlintv.MainViewModel
import com.dn.kotlintv.core.data.local.model.Episode
import com.dn.kotlintv.core.data.local.model.EpisodeHistory
import com.dn.kotlintv.core.data.local.model.Genre
import com.dn.kotlintv.core.data.local.model.Program

class Utils {
    companion object {
        var mFinishTimelineRequest: Boolean = false
        var mFinishRecommendRequest: Boolean = false
        var mFinishEditorChoiceRequest: Boolean = false
        var mFinishGenreRequest: Boolean = false
        var mFinishFavoriteRequest: Boolean = false
        var mFinishRecentlyRequest: Boolean = false
        var mProgramList = HashMap<String, Program>()
        var mRecommendationList = ArrayList<Episode>()
        var mEditorChoiceList = ArrayList<Program>()
        var mGenreList = ArrayList<Genre>()
        var mAllEpisodeList = ArrayList<Episode>()
        var mRecentlyHistoryList = ArrayList<EpisodeHistory>()
        var mRecentlyList = ArrayList<Episode>()
//        fun processData() {
//            if (mFinishTimelineRequest && mFinishRecommendRequest) {
//                Log.i("NAMTH","processData ne mProgramList=${mProgramList.size}")
//                Log.i("NAMTH","processData ne mRecommendationList=${mRecommendationList.size}")
////                Log.i("NAMTH","Process data here recommendationList="+MainViewModel.recommendationList.value?.data?.recommendations?.size)
//
//            }
//        }


    }

}