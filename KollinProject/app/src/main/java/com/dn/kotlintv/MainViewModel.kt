package com.dn.kotlintv

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.dn.kotlintv.core.data.local.model.GenreItem
import com.dn.kotlintv.core.data.local.model.Program
import com.dn.kotlintv.core.data.repository.TVRepository
import com.dn.kotlintv.core.data.response.EditorChoiceResponse
import com.dn.kotlintv.core.data.response.GenreResponse
import com.dn.kotlintv.core.data.response.RecommendResponse
import com.dn.kotlintv.core.data.response.TimelineResponse
import com.dn.kotlintv.utils.Resource
import kotlinx.coroutines.*
import java.lang.Exception
import kotlin.coroutines.CoroutineContext

class MainViewModel(var tvRepository: TVRepository) : ViewModel() {

    //create a new Job
    private val parentJob = Job()
    //create a coroutine context with the job and the dispatcher
    private val coroutineContext : CoroutineContext get() = parentJob + Dispatchers.Default
    //create a coroutine scope with the coroutine context
    private val scope = CoroutineScope(coroutineContext)

//    fun fetchTimeline
//    init {
//        Log.i("NAMTH", "GameViewModel created!")
//    }
////    companion object {
//    var mFinishTimelineRequest: Boolean = false
//    var mFinishRecommendRequest: Boolean = false
//    val allBlog = MutableLiveData<List<Program>>()
//    val allBlog1: LiveData<List<Program>> get() = getTimeline1()
//
//    var programList = MutableLiveData<Resource<TimelineResponse>>()
//    var recommendationList = MutableLiveData<Resource<RecommendResponse>>()
//    val completableJob = Job()
//    private val coroutineScope = CoroutineScope(Dispatchers.IO + completableJob)
////    }
//
//    fun observerTimeLine(): MutableLiveData<Resource<TimelineResponse>> {
//        return programList
//    }
//
//    fun observerRecommendation(): MutableLiveData<Resource<RecommendResponse>> {
//        return recommendationList
//    }
//
//    inner class GetTimeline : SingleObserver<TimelineResponse> {
//
//        override fun onSuccess(t: TimelineResponse) {
//            programList.postValue(Resource.success(t))
//            Log.i("NAMTH","onSuccess="+t.programList.size)
//            for (a: Program in t.programList) {
//                Log.i("NAMTH","title=".plus(a.title))
//            }
//            mFinishTimelineRequest = true
//
////            Utils.processData()
//        }
//
//        override fun onSubscribe(d: Disposable) {
//            Log.i("NAMTH","onSubscribe")
//            programList.postValue(Resource.loading())
//        }
//
//        override fun onError(e: Throwable) {
//            Log.i("NAMTH","onError")
//            mFinishTimelineRequest = true
//            programList.postValue(Resource.error(e))
//        }
//    }
//
//    fun getTimeline1() : MutableLiveData<List<Program>>{
//       coroutineScope.launch {
//           val request = tvRepository.getTimeLine()
//           withContext(Dispatchers.Main) {
//               try {
//                   val response = request.await()
//                   val programss = response
//                   if (programss != null && programss.programList != null) {
//                       allBlog.value = programss.programList
//                   }
//                    Log.i("NAMTH","getTimeline1 request")
//               }catch (e: HttpException) {
//
//               }catch (e: Throwable) {
//                   // Log error //)
//               }
//           }
//       }
//        return allBlog
//    }
//
//    inner class GetRecommendation : SingleObserver<RecommendResponse> {
//        override fun onSuccess(t: RecommendResponse) {
//            recommendationList.postValue(Resource.success(t))
//            Log.i("NAMTH","GetRecommendation onSuccess="+t.recommendations.size)
//            for (a: String in t.recommendations) {
//                Log.i("NAMTH","title= $a")
//            }
//            mFinishRecommendRequest = true
//
//            processData()
//        }
//
//        override fun onSubscribe(d: Disposable) {
//            recommendationList.postValue(Resource.loading())
//        }
//
//        override fun onError(e: Throwable) {
//            mFinishRecommendRequest = true
//            recommendationList.postValue(Resource.error(e))
//        }
//
//    }
//
//    fun getRecommendation() {
//        tvRepository.getRecommendation()
//            .subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribe(GetRecommendation())
//    }
//
//    fun processData() {
//
//    }
    lateinit var programList: MutableLiveData<List<Program>>
    fun fetchTimeline1() = liveData(Dispatchers.Default) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = tvRepository.requestTimeline()))

        } catch (e: Exception) {
            emit(Resource.error(data = null, message = e.toString()))
        }

    }

    fun fetchRecommendation() = liveData(Dispatchers.Default) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = tvRepository.getRecommendation()))
        } catch (e: Exception) {
            emit(Resource.error(data = null, message = e.toString()))
        }
    }

    suspend fun fetchRecommendation1() : RecommendResponse {
        val request = tvRepository.getRecommendation()
        return request
    }

    suspend fun fetchEditorChoice(): EditorChoiceResponse {
        return tvRepository.getEditorChoice()
    }

    suspend fun fetchGenre() : GenreResponse<List<GenreItem>> {
        return tvRepository.getGenre()
    }
    suspend fun fetchTimeline(): TimelineResponse<List<Program>> {
        val request = tvRepository.requestTimeline()
        return request
    }
}