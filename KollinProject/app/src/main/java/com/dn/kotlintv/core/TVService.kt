package com.dn.kotlintv.core

import com.dn.kotlintv.core.data.local.model.GenreItem
import com.dn.kotlintv.core.data.local.model.Program
import com.dn.kotlintv.core.data.response.EditorChoiceResponse
import com.dn.kotlintv.core.data.response.GenreResponse
import com.dn.kotlintv.core.data.response.RecommendResponse
import com.dn.kotlintv.core.data.response.TimelineResponse
import retrofit2.http.GET

interface TVService {

    @GET("1970310/raw")
//    suspend fun getTimeLine(): Deferred<TimelineResponse<Programs>>
//    suspend fun requestTimeline(): Deferred<TimelineResponse<List<Program>>>
    suspend fun requestTimeline(): TimelineResponse<List<Program>>

    @GET("1973651/raw")
    suspend fun getRecommendation(): RecommendResponse

    @GET("1979451/raw")
    suspend fun getEditorChoice(): EditorChoiceResponse

    @GET("1977417/raw")
    suspend fun getGenre(): GenreResponse<List<GenreItem>>
}