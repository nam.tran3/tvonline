package com.dn.kotlintv.screens.program

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.dn.kotlintv.R
import com.dn.kotlintv.core.data.local.model.Program
import kotlinx.android.synthetic.main.activity_video_player.view.*
import kotlinx.android.synthetic.main.episode_layout.view.*

class GenreAdapter(val context: Context) : BaseAdapter() {
    private var listProgram = ArrayList<Program>()
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var rootView: View;
        if (convertView == null) {
            rootView = LayoutInflater.from(context).inflate(R.layout.program_list_layout, null)
            var title = rootView.title_episode as TextView
            var created_episode = rootView.created_episode as TextView
            var thumbnail_episode = rootView.thumbnail_episode as ImageView
            title.text = listProgram.get(position).title
            created_episode.text = listProgram.get(position).createBy
            Glide
                .with(thumbnail_episode.getContext())
                .load(listProgram.get(position).logo)
                .thumbnail(
                    Glide
                        .with(thumbnail_episode.getContext())
                        .load(listProgram.get(position).logo)
                )
                .into(thumbnail_episode)
        } else {
            rootView = convertView
        }
        return rootView
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return listProgram.size
    }

    fun addItem(list: ArrayList<Program>) {
        listProgram = ArrayList<Program>(list)
    }
}