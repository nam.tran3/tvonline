package com.dn.kotlintv.screens.utils

import android.util.Log
import com.dn.kotlintv.core.data.local.model.Episode
import com.dn.kotlintv.core.data.local.model.EpisodeHistory
import com.dn.kotlintv.utils.Utils

class Utils {
    companion object {
        fun parseRecently(mRecentlyHistoryList: ArrayList<EpisodeHistory>, listAll: ArrayList<Episode>) {
            Utils.mRecentlyList.clear()
            Utils.mRecentlyList = ArrayList()
            if (!mRecentlyHistoryList.isEmpty()) {
                for (his in mRecentlyHistoryList) {
                    val videoId = his.video_id
                    for (e in listAll) {
                        if (videoId.equals(e.videoId)) {
                            Utils.mRecentlyList.add(e)
                        }
                    }
                }
            }
            Log.i("NAMTH", "parseRecently=" + Utils.mRecentlyList.size)
        }
    }
}