package com.dn.kotlintv.core.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.dn.kotlintv.core.data.local.model.EpisodeHistory

@Dao
interface EpisodeHistoryDao {
    @Query("Select * from history")
    fun getAll(): List<EpisodeHistory>

    @Query("Select * from history where videoId = :id")
    fun getEpisodeById(id: String): EpisodeHistory

    @Query("Select * from history order by createDate DESC")
    fun observerEpisodes(): LiveData<List<EpisodeHistory>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertEpisode(item: EpisodeHistory)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertEpisodeList(list: List<EpisodeHistory>)

    @Query("Delete from history where videoId = :id")
    fun deleteById(id: String)

    @Delete
    fun delete(episode: EpisodeHistory)

    @Query("Delete from history")
    fun deleteAll()
}