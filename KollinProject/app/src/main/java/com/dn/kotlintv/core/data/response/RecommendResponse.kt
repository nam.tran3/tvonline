package com.dn.kotlintv.core.data.response

import com.google.gson.annotations.SerializedName
class RecommendResponse {
    @SerializedName("recommendation")
    lateinit var recommendations : List<String>

}