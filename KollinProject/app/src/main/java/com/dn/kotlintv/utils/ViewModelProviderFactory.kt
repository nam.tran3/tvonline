package com.dn.kotlintv.utils

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dn.kotlintv.MainViewModel
import com.dn.kotlintv.core.data.repository.TVRepository
import java.security.Provider

class ViewModelProviderFactory (private val tvRepository: TVRepository) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MainViewModel(tvRepository) as T
    }

}